import React, {useState, useEffect} from 'react';
import './App.css';


const App = () => {
  const [year, setYear] = useState(1);
  const [month, setMonth] = useState(1);
  const [weekday, setWeekDay] = useState(1);
  const [day, setDay] = useState(1);
  const [hour, setHour] = useState(1);
  const [minute, setMinute] = useState(1);
  const [sec, setSec] = useState(1);
  const monthArray = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  const weekDayArray = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ];
  useEffect(() => {
    const interval = setInterval(() => {
      const date = new Date();
      setYear(date.getFullYear());
      setMonth(date.getMonth()+1);
      setWeekDay(date.getDay());
      setDay(date.getDate());
      setHour(date.getHours());
      setMinute(date.getMinutes());
      setSec(date.getSeconds());
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, []);


  // return clock(props);
  var array = length => Array.from({length}).map((i, v) => v + 1);
  const daysInMonth = (_year,_month) => {
    return new Date(_year,_month,0).getDate();
  }
  const _day = daysInMonth(year,month);

  return (<div>
    <div className="center_pos year">
      <div className="center_pos">
        {year}
      </div>
    </div>
    <div className="center_pos2">
      <div className="secNdl Ndl"
             style={{
               rotate : (((6) * sec) - 6) + 'deg',
               transformOrigin: 'left',
             }}></div>
    </div>
    <div className="center_pos2">
      <div className="minNdl Ndl"
           style={{
             rotate : (((6) * minute) - 6) + 'deg',
             transformOrigin: 'left',
           }}></div>
    </div>
    <div className="center_pos2">
      <div className="hrNdl Ndl"
           style={{
             rotate : (((15) * hour) - 15) + 'deg',
             transformOrigin: 'left',
           }}></div>
    </div>
    <div className="Month">{array(12).map((i, v) => {
      return (<div className={"center_pos month"}>
        <div style={{
          width: '210px',
          background: '',
          transform: 'rotate(' + ((30 * i) - (30 * month)) + 'deg)',
          color : (month == i ? '#F94D39' : ''),
          transition  : 'transform .9s',


        }}>
          {monthArray[v]}
        </div>
      </div>);
    })}
    </div>
    <div className="WeekDay">{array(7).map((i, v) => {
      return (<div className={"center_pos weekday"}>
        <div style={{
          width: '410px',
          background: '',
          transform: 'rotate(' + ((51 * i) - (51 * weekday)) + 'deg)',
          color : (weekday == i ? '#F94D39' : ''),
          transition  : 'transform .9s',


        }}>
          {weekDayArray[v]}
        </div>
      </div>);
    })}</div>
    <div className="Day">{array(_day).map((i, v) => {
      return (<div className={"center_pos day"}>
        <div style={{
          width: '550px',
          background: '',
          transform: 'rotate(' + ((360/_day * i) - (360/_day * day)) + 'deg)',
          color : (day == i ? '#F94D39' : ''),
          transition  : 'transform .9s',


        }}>
          Day {i}
        </div>
      </div>);
    })}
    </div>
    <div className="Hour">{array(24).map((i, v) => {
      return (<div className={"center_pos hour"}>
        <div style={{
          width: '730px',
          background: '',
          transform: 'rotate(' + (((15) * i) - ((15))) + 'deg)',
          color : (hour == i ? '#F94D39' : ''),
          transition  : 'transform .9s',


        }}>
          {i} Hour{(i > 1) ? 's' : ''}
        </div>
      </div>);
    })}
    </div>
    <div className="Minute">{array(60).map((i, v) => {
      return (<div className={"center_pos minute"}>
        <div style={{
          width: '930px',
          background: '',
          transform: 'rotate(' + (((6) * i) - ((6))) + 'deg)',
          color : (minute == i ? '#F94D39' : ''),
          transition  : 'transform .9s',


        }}>
          {i} Minute{(i > 1) ? 's' : ''}
        </div>
      </div>);
    })}
    </div>
    <div className="Second">{array(60).map((i, v) => {
      return (<div className={"center_pos sec"}>
        <div style={{
          width: '1090px',
          background: '',
          transform: 'rotate(' + (((6) * i) - ((6))) + 'deg)',
          color : (sec == i ? '#F94D39' : ''),
          transition  : 'transform .9s',
        }}>
          {i} Sec{(i > 1) ? 's' : ''}
        </div>
      </div>);
    })}
    </div>
  </div>);

}
export default App;
